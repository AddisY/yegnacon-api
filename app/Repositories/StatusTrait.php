<?php
/**
 * Created by PhpStorm.
 * User: Mohammed
 * Date: 3/13/2016
 * Time: 5:52 PM
 */

namespace App\Repositories;


trait StatusTrait
{
    public $vacancyStatus = 'active';
    public $userStatus = 'active';
    public $tenderStatus = 'active';
    public $activeProjectStatus = 'active';
    public $newsStatus = 'active';
    public $proformaRequestStatus = 'active';
}